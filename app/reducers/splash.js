import {
    GETHOME_FAILURE,
    GETHOME_SUCCESS,
} from '../action/splash';

export default function home(state = {
    home: {},
}, action) {
    switch (action.type) {
        case GETHOME_FAILURE:
            return {
                ...state,
                error: action.error
            };
		case GETHOME_SUCCESS:
            return {
                ...state,
                home: action.result,
            };
        default:
            return state
    }
};