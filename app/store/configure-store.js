import {createStore,applyMiddleware,compose} from 'redux';
import thunkMiddleware from 'redux-thunk';
import rootReducer from '../reducers/rootReducers';
import createLogger from 'redux-logger';

    const logger = createLogger();
    let Middleware=[];
    if(process.env.NODE_ENV=='production'){
        Middleware=[thunkMiddleware]
    }else{
        Middleware=[thunkMiddleware,logger]
    }


    const createStoreWithMiddleware =
        compose(
            applyMiddleware(
                ...Middleware
            ),
            window.devToolsExtension ? window.devToolsExtension() : f => f
        )(createStore);

export default function configureStore(initialState) {
    return createStoreWithMiddleware(rootReducer, initialState);
}

